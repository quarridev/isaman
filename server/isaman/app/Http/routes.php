<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::controller('auth', 'Auth/UsersController');

//Route::get('/users', 'UserController@index',['middleware' => 'auth']);
//Route::controller('users', 'UserController',['middleware' => 'auth']);
Route::controllers([
	 'auth' => 'Auth\AuthController',
	 'users' => 'UsersController'
]);
/*Route::post('/user', 'UserController@store');
Route::get('/user/create', 'UserController@create');
Route::get('/user/edit/{id}', 'UserController@edit');
Route::get('/user/{id}', 'UserController@show');
Route::put('/user/{id}', 'xUserController@update');
Route::delete('/user/{id}', 'UserController@destroy');*/
//Route::get('/login', 'serController@login');

/*Route::get('login', array(
  'uses' => 'RegisterController@create',
  'as' => 'register.create'
));
Route::post('login', array(
  'uses' => 'UsersController@store',
  'as' => 'user.login'
));
Route::get('logout', array(
  'uses' => 'RegisterController@destroy',
  'as' => 'register.destroy'
));*/

//Route::controller('/login', 'Auth/UsersController@getIndex');
Route::get('/', 'WelcomeController@index');
