<?php namespace iSaman\Http\Controllers;

use iSaman\User;
use iSaman\Http\Requests;
use iSaman\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class UsersController extends Controller {

    /**
      * @var User
      */
     protected $user;
  
     /**
      * @param User $user
      */
     public function __construct(User $user)
     {
         $this->user = $user;
         
     }


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex()
	{
		
       $users = $this->user->all();
 
        return view('users.index')->with(compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return  \Illuminate\View\View
	 */
	public function getCreate()
	{
	    return view('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * @param Request $request
	 * @return  \Illuminate\Http\RedirectResponse
	 */
	public function postCreate(Request $request)
	{
       
       $v = $this->validate($request, [
            'name' => 'required|',
            'email' => 'required|email|unique:users',
            'phoneno' => 'required|unique:users|max:100',
            ]);


        if ($v)
        {
          //return redirect()->back()->withErrors($v->errors());
        	//Session::flash('message', $v);
        	return Redirect::back()->withInput();
        }

        $data = $request->all();
        $this->user->fill($data);
        $this->user->save();
 
       return redirect()->to('users');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\View\View
	 */
	public function getShow($id)
	{
	    $user = $this->user->findOrFail($id);
        return view('users.show')->with(compact('user'));   
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return  \Illuminate\View\View
	 */
	public function getEdit($id)
	{
        $user = $this->user->findOrFail($id);
        return view('users.edit')->with(compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 * @param Request $request
	 * @param  int  $id
	 * @return  \Illuminate\View\View
	 */
	public function postEdit($id, Request $request)
	{
        $user = $this->user->findOrFail($id);
       
        $data = $request->all();

        $user->fill($data);
        $user->password = bcrypt($user->password);
        $user->save();
      
       return redirect('users/show/'.$id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return  \Illuminate\Http\RedirectResponse
	 */
	public function deleteUser($id)
	{
        $user = $this->user->findOrFail($id);
        $user->delete();

        return redirect()->to('users');
	}

}
