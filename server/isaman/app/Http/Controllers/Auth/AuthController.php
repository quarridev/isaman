<?php namespace iSaman\Http\Controllers\Auth;

use iSaman\User;
use iSaman\Http\Requests;
use iSaman\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Auth;


class AuthController extends Controller {

    /**
     * Handle an authentication attempt.     *
     * @return  \Illuminate\View\View
     */
    public function getIndex()
    {
       
         return view('auth.login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\View\View
     */
    public function postLogin(Request $request)
    {
        $v = $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|',
        ]);

        if ($v)
        {
          //return redirect()->back()->withErrors($v->errors());
          //Session::flash('message', $v);
          return Redirect::back()->withInput();
        }
      
        $data = $request->all();
         
    
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]))
        {
            //return redirect()->intended('dashboard');
            return redirect()->intended('users');
        }    
 
         
    }
   

}