@extends('layouts.app')
  
  @section('content')
			      
      <h2 class="page-header">LOGIN</h2>
      <form action="{{ URL::to('auth/login') }}" method="POST">
            <div class="form-group">
              <label>Email</label>
             <input class="form-control"  type="email" name="email" required placeholder="Enter email"/>
          </div>
          <div class="form-group">
              <label>Password</label>
             <input class="form-control"  type="password" name="password" required placeholder="Enter password"/>
          </div>
          <button type="submit" class="btn btn-primary">Login</button>

          <input type="hidden" name="_token" value="{{ csrf_token() }}">
      </form> 

 @endsection