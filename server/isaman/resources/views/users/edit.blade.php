@extends('layouts.app')
  
  @section('content')

      <h2 class="page-header">UPDATE {{ $user->name }}</h2>
      <form action="{{ URL::to('users/edit', $user->id) }}" method="POST">
          <div class="form-group">
              <label>Name</label>
              <input class="form-control" value="{{ $user->name }}" type="text" name="name" required placeholder="Enter name"  />
          </div>
          <div class="form-group">
              <label>Email</label>
             <input class="form-control" value="{{ $user->email }}" type="email" name="email" required placeholder="Enter email"/>
          </div>
          <div class="form-group">
              <label>Phone No</label>
             <input class="form-control" value="{{ $user->phoneno }}"  type="tel" name="phoneno" required placeholder="Enter phone no"/>
          </div>
          <button type="submit" class="btn btn-primary">Save</button>

          <a href="{{ URL::to('users') }}" class="btn btn-danger pull-right">Cancel</a>
          
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
      </form>
  @endsection