@extends('layouts.app') @section('content')

<h1>All Users</h1>

<p><a href="{{ URL::to('users/create') }}">Create a user</a>
</p>

@if ($users->count())
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Last Created</th>
            <th>Last Updated</th>

        </tr>
    </thead>

    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>

            <a class="text-info" href="{{ URL::to('users/show', array($user->id)) }}">{{ $user->name }}</a>
            </td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phoneno }}</td>
            <td>{{{ $user->created_at }}}</td>
            <td>{{{ $user->updated_at }}}</td>

            <td><a class="btn btn-info" href="{{ URL::to('users/edit', array($user->id)) }}">Edit</a>
            </td>

            <td>

                <form class="pull-right" method="POST" action="{{URL::to('users', array($user->id))}}">

                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-warning">Delete</button>
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>

            </td>
        </tr>
        @endforeach

    </tbody>

</table>
@else There are no users @endif @stop
