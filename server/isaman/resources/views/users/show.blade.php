@extends('layouts.app')
  
  @section('content')

      <h2 class="page-header">{{ $user->name }}</h2>

      <table class="table table-striped">
          <tbody>
          <tr>
              <th>Name</th>
              <td>{{{ $user->name }}}</td>
          </tr>
          <tr>
              <th>Email</th>
              <td>{{{ $user->email }}}</td>
          </tr>
             <tr>
          <th>Phone no</th>
              <td>{{{ $user->phoneno }}}</td>
           </tr>
           <tr>
          <th>Point Accumulated</th>
              <td>{{{ $user->points }}}</td>
          </tr>
          <tr>
              <th>Last Created</th>
              <td>{{{ $user->created_at }}}</td>
          </tr>
          <tr>
              <th>Last Updated</th>
              <td>{{{ $user->updated_at }}}</td>
          </tr>
          </tbody>
      </table>

   <table class="table table-condensed">
     <td>
     <a href="{{ URL::to('users') }}" class="btn btn-default">List</a>
     <a href="{{ URL::to('users/create') }}" class="btn btn-default">Add</a>
     <a class="btn btn-default" href="{{ URL::to('users/edit', array($user->id)) }}">Edit</a>
            </td>

            <td>

                <form class="pull-right" method="POST" action="{{URL::to('users', array($user->id))}}">

                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-warning">Delete</button>
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
            </td>

      </table>

  @endsection