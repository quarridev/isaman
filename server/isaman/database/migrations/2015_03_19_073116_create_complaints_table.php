<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('complaints', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('contact')->default('');
			$table->string('email')->default('');
			$table->string('phoneno')->default('');
			$table->string('plateno')->default('');
			$table->string('address')->default('');
			$table->string('location_lat')->default('');
			$table->string('location_lng')->default('');
			$table->smallInteger('offence_id')->nullable();
			$table->string('offense_desc')->default('')->nullable();
			$table->boolean('valid')->nullable();
			$table->string('audit_by')->default('')->nullable();
			$table->string('note')->default('')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('complaints');
	}

}
