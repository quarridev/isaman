<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
		    $table->string('name')->default('');
			$table->string('email')->default('');
			$table->string('phoneno')->default('')->nullable();
			$table->string('password')->default('');
			$table->boolean('resetpw')->nullable();
			$table->rememberToken();
			$table->string('email_code',100)->default('')->nullable();
			$table->string('sms_code',100)->default('')->nullable();
			$table->boolean('email_verified')->nullable();
			$table->boolean('sms_verified')->nullable();
			$table->bigInteger('role_id')->default(1);
			$table->bigInteger('created_by')->nullable();
			$table->bigInteger('updated_by')->nullable();
			$table->bigInteger('points')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
