angular.module('starter.services', [])

.factory('imgURI', function () {
      var img = {
        uri: ''
    };

    return {
        getUri: function () {
            return img.uri;
        },
        setUri: function (uri) {
            img.uri = uri;
        }
    };
})
.factory('Report', function () {
      var lc_report = {
        vehicleNumber: '',
        imgURI: '',
        geolocation:{lat:0,lng:0},
        datetime:'',
        reporter: {name:'',id:'',idType:''},
    };

    return {
        getReport: function () {
            return lc_report;
        },
        setReport: function (report) {
            lc_report= report;
        }
    };
})
.service('LicenseIdService', function($q) {
    return {
        readId: function(uri) {
            var deferred = $q.defer();
            var promise = deferred.promise;
 
                deferred.resolve("ABC8888");
       
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
})
.service('LoginService', function($q) {
    return {
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;
 
            if (name == 'user' && pw == '123') {
                var token= 123;
                deferred.resolve({name: name, token:token});
            } else {
                deferred.reject('Wrong credentials.');
            }
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
});
