var _offences = [{
        title: 'Double Park',
        id: 1
    }, {
        title: 'Traffic Obstruction',
        id: 2
    }, {
        title: 'Park Over Lane',
        id: 3
    },

];
var _placeholder = "./img/camera.png";
angular.module('starter.controllers', [])

.controller('AppCtrl', function($rootScope, $scope, $state, $ionicModal, $ionicPopup, $timeout, LoginService) {




        // Form data for the login modal
        $scope.loginData = {};
        $scope.regData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function() {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function() {
            $scope.modal.show();
        };



        // Perform the login action when the user submits the login form
        $scope.doLogin = function(welcome) {
            //console.log('Doing login', $scope.loginData);




            LoginService.loginUser($scope.loginData.username, $scope.loginData.password).success(function(data) {
                //$rootScope.currentUser = data;
                window.localStorage['auth'] = data;

                $rootScope.currentUser = data;


                $state.go('app.offences');
            }).error(function(data) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Login failed!',
                    template: 'Please check your credentials!'
                });
            });

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            if (!welcome) {
                $timeout(function() {
                    $scope.closeLogin();
                }, 1000);

            }
        };


        $scope.logout = function() {
            $rootScope.currentUser = 'undefined';
            window.localStorage['auth'] = {};
            $state.go('welcome');
        };
        $scope.signup = function() {
            console.log('Signup');
            $state.go('registration');
        };



    })
    .controller('RegisterCtrl', function($rootScope, $scope, $state, $ionicModal, $ionicPopup, $timeout) {


    })
    .controller('OffencesCtrl', function($scope, $ionicPopup, $location, $cordovaCamera, imgURI) {
        $scope.offences = _offences;


        $scope.selected = function(offenseId) {

            $location.path("/app/offences/" + offenseId);
        };



    })

.controller('ProfileCtrl', function($rootScope, $scope, $state, $ionicModal, $ionicPopup, $timeout) {

    // Submit Function here
    $scope.alertNoFunction = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Warning!',
            template: 'This function not implemented yet!'
        });
        alertPopup.then(function(res) {
            console.log('');
        });
    };




    $scope.profileData = window.localStorage['auth'] || {};

    $ionicModal.fromTemplateUrl('templates/changepw.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.changepw = function() {
        $scope.modal.show();
    };
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };
})


.controller('OffenceCtrl', function($scope, $state, LicenseIdService, GeoCoder, $timeout, $ionicPopup, $stateParams, $cordovaCamera, $ionicLoading, $ionicActionSheet, $cordovaGeolocation, imgURI, Report) {



    $scope.$root.showSubmit = true;
    $scope.offences = _offences;
    $scope.plateId = "";
    $scope.offenseId =
        $scope.selected = _offences[$stateParams.offenceId - 1].title;
    $scope.imgURI = _placeholder;

    $scope.data = {
        timestamp: '',
        photoUri: '',
        location: '',
        address: '',
        platenId: '',
        offenceId: $stateParams.offenceId,
        offenceDesc: $scope.selected,
        contact: 'Jay Chow',
        email: 'me@isaman.net',
        mobileno: '+60188808888'
    };

    $scope.scanPlate = function() {

        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 30,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: true
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {

            $timeout(function() {

                //$scope.imgURI = imageData;
                $scope.submitPlate(imageData);

            }, function(err) {
                // An error occured. Show a message to the user
            });
            return true;

        });
    };

    $scope.submitPlate = function(uri) {

        $ionicLoading.show({
            template: 'Scanning...'
        });

        LicenseIdService.readId(uri).success(function(Id) {
            $scope.plateId = Id;
            $ionicLoading.hide();
        }).error(function(data) {

            var alertPopup = $ionicPopup.alert({
                title: 'Offline',
                template: 'Please check your connection!'
            });
        });

    };
    $scope.takePicture = function() {


        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            //targetWidth: 300,
            //targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: true
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {



            $timeout(function() {

                /* Uri imgUri=Uri.parse("android.resource://my.package.name/"+R.drawable.image);
                 imageView.setImageURI(null); 
                 imageView.setImageURI(imgUri);
               */
                // data.photoUri = imgURI.setUri("data:image/jpeg;base64," + imageData);
                 //$scope.imgURI = data.photoUri;
                 $scope.imgURI = "data:image/jpeg;base64," + imageData;

                //$scope.imgURI = "data:image/jpeg;base64," + imageData;
                //$scope.imgURI = imageData;
            }, 2000);
            /*if(!$scope.$$phase) {
               $scope.$apply(function() { 
                  });
            }*/
           // $scope.imgURI = imgURI.getUri();


        }, function(err) {
            // An error occured. Show a message to the user

        });
        return true;

    };

    $scope.$on('mapInitialized', function(event, map) {

        $scope.map = map;
    });

    $scope.position = {};

    var posOptions = {
        timeout: 20000,
        enableHighAccuracy: true,

    };

 
   $ionicLoading.show({
       template: 'Fixing Location...'
     });

    $scope.address = "";
    

    $cordovaGeolocation.getCurrentPosition(posOptions)
        .then(function(position) {
            $scope.position = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            $scope.positions = [{
                lat: position.coords.latitude,
                lng: position.coords.longitude
            }];
            var marker = new google.maps.Marker({title:'My Position'});
            var latlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);

            marker.setPosition(latlng);
            marker.setMap($scope.map);


            //var latlng = new google.maps.LatLng(lat, lng);

            
             


        }).catch(function(err){
          $ionicPopup.alert({
                    title: 'Geo Location service',
                    template: 'Timeout due to: ' + err
                });

        }).finally(function(){
            if($scope.position){
               var lat = parseFloat($scope.position.lat);
               var lng = parseFloat($scope.position.lng);
               $scope.getGeoCoding(lat, lng);
            }else{
                $ionicLoading.hide();
            }
        });
    $scope.getGeoCoding = function(lat, lng) {
        GeoCoder.geocode({
            'latLng': {
                lat: lat,
                lng: lng
            }
        }).then(function(result) {
            $scope.address = result[0].formatted_address;
           
        }).catch(function(err){
        
              $ionicPopup.alert({
                    title: 'GeoCoder',
                    template: 'Address unknown due to: ' + err
                });
           

        }).finally(function(){
            $ionicLoading.hide();
        });

    }


    // Submit Function here
    $scope.$root.alertNoFunction = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Warning!',
            template: 'This function not implemented yet!'
        });
        alertPopup.then(function(res) {
            console.log('');
        });
    };



    $scope.$on("$stateChangeStart", function() {
        $scope.$root.showSubmit = false;

    });




});
