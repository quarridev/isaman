// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ui.router', 'ngMap', 'starter.controllers', 'starter.services', 'ngCordova'])

.run(function($ionicPlatform, $rootScope, $state) {
    $ionicPlatform.ready(function() {

        //Solve and clear annoying ripple warn dialog. Comment if on device
        var annoyingDialog = parent.document.getElementById('exec-dialog');
        if (annoyingDialog) annoyingDialog.outerHTML = "";

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();

        }
        //Test if camera loaded
        if (window.cordova && window.cordova.plugins.Camera) {
            
        }

    });

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        var requireLogin = toState.data.requireLogin;

        if(window.localStorage['auth']){
                $rootScope.currentUser = window.localStorage['auth'];
        }

        if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
            event.preventDefault();
            return $state.go('welcome');
        }

    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('welcome', {
        url: "/welcome",
        abstract: false,
        templateUrl: "templates/welcome.html",
        controller: 'AppCtrl',
        data: {
            requireLogin: false
        }
    })

    .state('registration', {
        url: "/registration",
        abstract: false,
        templateUrl: "templates/registration.html",
        controller: 'RegisterCtrl',
        data: {
            requireLogin: false
        }
    })


    .state('app', {
        url: "/app",
        abstract: false,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl',
        data: {
            requireLogin: true,
        }
    })
    .state('app.profile', {
            url: "/profile",
            views: {
                'menuContent': {
                    templateUrl: "templates/profile.html",
                    controller: 'ProfileCtrl'
                }
            },
            data: {
                requireLogin: true,
            }
        })

    .state('app.about', {
            url: "/about",
            views: {
                'menuContent': {
                    templateUrl: "templates/about.html"
                }
            },
            data: {
                requireLogin: true,
            }
        })
          .state('app.capture', {
            url: "/capture/:offenceId",
            views: {
                'menuContent': {
                    templateUrl: "templates/capture.html",
                    controller: 'PlateCtrl'
                }
            },
            data: {
                requireLogin: true,
            }
        })
        .state('app.offences', {
            url: "/offences",
            views: {
                'menuContent': {
                    templateUrl: "templates/offences.html",
                    controller: 'OffencesCtrl'
                }
            },
            data: {
                requireLogin: true,
            }
        })

    .state('app.offence', {
        url: "/offences/:offenceId",
        views: {
            'menuContent': {
                templateUrl: "templates/offence.html",
                controller: 'OffenceCtrl'
            },
            data: {
                requireLogin: true,
            }
        }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/welcome');

});
